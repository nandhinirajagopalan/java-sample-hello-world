public class HelloWorld {
    public static void main(String[] args) {
        int exitCode = 0;
        System.out.println("Hello, World Update");

        //Setting it to 1 manually, this is not always the case
        if(true){
            exitCode = 1;
        }
        System.exit(exitCode);
    }
}
